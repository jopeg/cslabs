""" 
file: hashtable.py
language: python3
author: sps@cs.rit.edu Sean Strout 
author: jeh@cs.rit.edu James Heliotis 
author: anh@cs.rit.edu Arthur Nunes-Harwitt

Student: Joseph Gambino <jdg7026@rit.edu>

description: Chaining hash Table for CS 242 Lab
"""

class HashTable( object ):
    """
       The HashTable data structure contains a collection of values
       where each value is located by a hashable key.
       No two values may have the same key, but more than one
       key may have the same value.
    """

    __slots__ = ( "table", "size" )

    def __init__( self, capacity=89 ):
        """
           Create a hash table.
           The capacity parameter determines its initial size.
        """
        self.table = [None] * capacity
        self.size = 0

    def __str__( self ):
        """
           Return the entire contents of this hash table,
           one chain of entries per line.
        """
        result = ""
        for i in range( len( self.table ) ):
            result += str( i ) + ": "
            result += str( self.table[i] ) + "\n"
        return result

    class _Entry( object ):
        """
           A nested class used to hold key/value pairs.
        """

        __slots__ = ( "key", "value" )

        def __init__( self, entryKey, entryValue ):
            self.key = entryKey
            self.value = entryValue

        def __str__( self ):
            return "(" + str( self.key ) + ", " + str( self.value ) + ")"

def imbalance(hTable):
    total = 0
    nonEmpty = 0
    for index in hTable.table:
        if index == None: 
            pass
        else:
            total += len(index)
            nonEmpty += 1
    return (total / nonEmpty) - 1

def hash_function( key, hashSize ):
    keyCode = 0
    pre = 0
    for c in key:
        keyCode += ord(c) + (pre * keyCode * 54 *  ord(c))
        pre = ord(c)

    hashCode = (len(key) + keyCode) % hashSize
    return hashCode

#Original Hash
# def hash_function( val, n ):
#     hashcode = len(val) % n
#     return hashcode


def keys( hTable ):
    """
       Return a list of keys in the given hashTable.
    """
    result = []
    for entry in hTable.table:
        if entry != None:
            for key in entry:
              result.append( key.key )
    return result

def contains( hTable, key ):
    """
       Return True iff hTable has an entry with the given key.
    """
    index = hash_function( key, len( hTable.table ) )
    if hTable.table[index] == None:
        return False
    else: 
        for entry in hTable.table[index]:
            if entry.key == key:
                return True
        return False

def put( hTable, key, value ):
    """
       Using the given hash table, set the given key to the
       given value. If the key already exists, the given value
       will replace the previous one already in the table.
       If the table is full, an Exception is raised.
    """ 

    if hTable.size/len(hTable.table) >= 0.75:
        resize(hTable)

    index = hash_function( key, len( hTable.table ) )
    if hTable.table[index] == None:
        hTable.size+=1
        hTable.table[index] = [hTable._Entry(key, value)]
    else:
        exists = False
        for entry in hTable.table[index]:
            if entry.key == key:
                exists = True
        if exists:
            for entry in hTable.table[index]:
                if entry.key == key:
                    entry.value = value
        else:
            hTable.size+=1
            hTable.table[index].append(hTable._Entry(key, value))

def get( hTable, key ):
    """
       Return the value associated with the given key in
       the given hash table.
       Precondition: contains(hTable, key)
    """
    index = hash_function( key, len( hTable.table ) )
    if hTable.table[index] == None:
        raise Exception( "Hash table does not contain key." )
    else: 
        for entry in hTable.table[index]:
            if entry.key == key:
                return entry.value
        raise Exception( "Hash table does not contain key." )

def resize(hTable):
    newTable = HashTable(2*(len(hTable.table)) + 1)
    for index in hTable.table:
        if index == None:
            pass
        else:
            for entry in index:
                put(newTable, entry.key, entry.value)
    hTable.table = newTable.table