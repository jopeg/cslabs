'''
file: balloons.py
language: python3
assignment: Magical Balloons Lab
author: Joseph Gambino <jdg7026@rit.edu>
'''

import array_heap as heap
from math import *

def main():
	balloonList = []
	balloonDict = {}
	fileName = input("File Name: ")
	print(fileName)
	for line in open(fileName):
		line = line.split()
		if len(line) < 4:
			balloons = heap.Heap(int(line[0])**2, compare)
		else:
			balloonList.append((line[0],int(line[1]),int(line[2]),int(line[3])))
	index = 0
	while index < len(balloonList):
		balloon = balloonList[index]
		for balloon2 in balloonList[index:]:
			if balloon2 != balloon:
				dist = sqrt((balloon2[1]-balloon[1])**2 + (balloon2[2]-balloon[2])**2 + (balloon2[3]-balloon[3])**2)
				if dist in balloonDict:
					balloonDict[dist].append([balloon, balloon2])
				else: 
					balloonDict[dist] = [[balloon, balloon2]]
		index += 1

	for key in balloonDict:
		heap.add(balloons, (key, balloonDict[key]))

	while len(balloonList) > 1:
		temp = []
		distSet = heap.removeMin(balloons)
		for pair in distSet[1]:
			if pair[0] in balloonList and pair[1] in balloonList:
				temp.append(pair[0])
				temp.append(pair[1])
		for balloon in temp:
			if balloon in balloonList:
				balloonList.remove(balloon)
	if len(balloonList) == 0:
		print("NO")
	else:
		print(balloonList[0][0] + " at location (" + str(balloonList[0][1]) + "," + str(balloonList[0][2]) + "," + str(balloonList[0][3]) + ") will expand indefinately.")

def compare(a,b):
	return a[0] < b[0]

if __name__ == '__main__':
	main()
