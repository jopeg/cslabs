/**
 * CardButton.java
 *
 * File:
 *	$Id: CardButton.java,v 1.4 2013/04/22 18:56:03 jdg7026 Exp $
 *
 * Revisions:
 *	$Log: CardButton.java,v $
 *	Revision 1.4  2013/04/22 18:56:03  jdg7026
 *	comments added
 *
 *	Revision 1.3  2013/04/22 06:05:58  jdg7026
 *	pretty much done with everything except cheating
 *
 *	Revision 1.2  2013/04/22 02:39:11  jdg7026
 *	started  implementing listeners and what not
 *
 *	Revision 1.1  2013/04/22 02:20:45  jdg7026
 *	first commit just started
 *
 */

import javax.swing.*;
import java.awt.*;

/**
 * Card Button jframe
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public class CardButton extends JButton{
	private int pos;
	
	CardButton(int pos){
		this.pos = pos;
		//sets some default options
		this.setBorderPainted(false);
		this.setContentAreaFilled(false);
		this.setOpaque(true);
		this.setForeground(Color.WHITE);
	}
	
	public int getPos(){
		return this.pos;
	}
}