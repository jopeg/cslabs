/**
 * CheatFrame.java
 *
 * File:
 *	$Id: CheatFrame.java,v 1.3 2013/04/23 02:59:53 jdg7026 Exp $
 *
 * Revisions:
 *	$Log: CheatFrame.java,v $
 *	Revision 1.3  2013/04/23 02:59:53  jdg7026
 *	re-implemented the cheat frame to be the way the javadocs say. oops.
 *
 *	Revision 1.2  2013/04/22 18:56:03  jdg7026
 *	comments added
 *
 *	Revision 1.1  2013/04/22 18:30:12  jdg7026
 *	finished everything, all that remains are comments
 *
 *
 */

import java.awt.*;

import javax.swing.*;
import java.util.*;
import java.awt.color.*;

/**
 * Cheat frame for concentration game
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public class CheatFrame extends JFrame{
	
	/**
	 * Constructs a new cheat frame based on the model
	 * @param model - Concentration Model
	 * @param cardColors - array list of card colors
	 */
	public CheatFrame(ArrayList<CardButton> cards, int size){
		setLayout(new GridLayout(4,4,2,2));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Cheat Concentration Game");
		setSize(300,300);
		setLocationRelativeTo(null);
		for( int i = 0; i < cards.size(); i++ )
			this.add(cards.get(i));
		setVisible(true);
	}
}