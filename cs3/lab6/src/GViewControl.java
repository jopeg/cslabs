/**
 * GViewControl.java
 *
 * File:
 *	$Id: GViewControl.java,v 1.7 2013/04/23 02:59:53 jdg7026 Exp $
 *
 * Revisions:
 *	$Log: GViewControl.java,v $
 *	Revision 1.7  2013/04/23 02:59:53  jdg7026
 *	re-implemented the cheat frame to be the way the javadocs say. oops.
 *
 *	Revision 1.6  2013/04/22 18:56:03  jdg7026
 *	comments added
 *
 *	Revision 1.5  2013/04/22 18:30:12  jdg7026
 *	finished everything, all that remains are comments
 *
 *	Revision 1.4  2013/04/22 06:05:57  jdg7026
 *	pretty much done with everything except cheating
 *
 *	Revision 1.3  2013/04/22 03:47:31  jdg7026
 *	starting construction of UI
 *
 *	Revision 1.2  2013/04/22 02:39:11  jdg7026
 *	started  implementing listeners and what not
 *
 *	Revision 1.1  2013/04/22 02:20:45  jdg7026
 *	first commit just started
 *
 */

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

/**
 * View for concentration game
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public class GViewControl extends JFrame implements Observer{
	private ConcentrationModel model;
	private JLabel moveCount;
	private ArrayList<Color> cardColors;
	private ArrayList<CardButton> cardButtons;
	private JPanel cardButtonPanel;
	private JPanel bottomBar;
	private GViewControl game;
	
	/**
	 * constructs a new GViewControl based on the model
	 * and initializes some default window options
	 * 
	 * @param model - ConcentrationModel that this view
	 * 				is displaying
	 */
	GViewControl(ConcentrationModel model){
		super();
		this.game = this;
		model.addObserver(this);
		this.model = model;
		this.setLayout(new BorderLayout());
		
		//creates move count label
		this.moveCount = new JLabel("Moves: ");
		add(moveCount, BorderLayout.NORTH);
		
		//creates card button jpanel, and populates it with buttons
		this.cardButtons = new ArrayList<CardButton>();
		this.cardButtonPanel = new JPanel();
		this.cardButtonPanel.setLayout(new GridLayout(4,4,2,2));
		
		for( int i = 0; i < this.model.NUM_CARDS; i++ ){
			CardButton newButton = new CardButton(i);
			newButton.addActionListener(new JButtonListener());
			this.cardButtons.add(newButton);
			this.cardButtonPanel.add(newButton);
		}
			
		//sets card color array so each number gets assigned to a color
		this.cardColors = new ArrayList<Color>();
		this.cardColors.add(Color.BLUE);
		this.cardColors.add(Color.GREEN);
		this.cardColors.add(Color.MAGENTA);
		this.cardColors.add(Color.ORANGE);
		this.cardColors.add(Color.CYAN);
		this.cardColors.add(Color.RED);
		this.cardColors.add(Color.LIGHT_GRAY);
		this.cardColors.add(Color.PINK);
			
		add(cardButtonPanel, BorderLayout.CENTER);
		
		//creates bottom bar and buttons for reset cheat and undo
		this.bottomBar = new JPanel();
		this.bottomBar.setLayout(new FlowLayout(FlowLayout.RIGHT));
		JButton reset = new JButton("Reset");
		reset.addActionListener(new ResetListener());
		JButton cheat = new JButton("Cheat");
		cheat.addActionListener(new CheatListener());
		JButton undo = new JButton("Undo");
		undo.addActionListener(new UndoListener());
		this.bottomBar.add(reset);
		this.bottomBar.add(cheat);
		this.bottomBar.add(undo);
		
		add(bottomBar, BorderLayout.SOUTH);
		
		// sets some options for the window, like default size and title
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Concentration Game");
		setSize(400,400);
		setLocationRelativeTo(null);
		setVisible(true);
		// updates the view based on the initial model
		this.update(this.model, this);
		
	}
	
	/**
	 * creates a new view, and gives it a new model when the program is run
	 * @param args - command line arguments, not used
	 */
	public static void main(String[] args){
		new GViewControl(new ConcentrationModel()); 
	}
	
	/**
	 * updates the view based on the model.
	 * @param t - observable object, not used
	 * @param o - observer object, not used
	 */
	public void update(Observable t, Object o){
		// sets the move counter
		int numSelected = this.model.howManyCardsUp();
		String select = (numSelected == 1)? "second":"first";
		int moves = this.model.getMoveCount();
		this.moveCount.setText("Moves:  " + moves + "Select the " + select + " card.");

		// sets cards either face up or face down
		ArrayList<CardFace> cards = this.model.getCards();
		for( int i = 0; i < model.NUM_CARDS; i++ ){
			if ( cards.get(i).isFaceUp() ){
				int cardNum = cards.get(i).getNumber();
				Color color = this.cardColors.get(cardNum);
				this.cardButtons.get(i).setBackground(color);
				this.cardButtons.get(i).setText(""+cardNum);
			} else {
				this.cardButtons.get(i).setBackground(Color.WHITE);
				this.cardButtons.get(i).setText("");
			}
		}	
		// redraws the screen
		this.validate();
	}
	/**
	 * inner class for a JButton listener for card button.
	 */
	public class JButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			//selects the card the user clicked in the model
			CardButton button = (CardButton)e.getSource();
			model.selectCard(button.getPos());
		}
	}
	
	/**
	 * inner class for a reset button
	 */
	public class ResetListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			//resets the game in the model
			model.reset();
		}
	}
	
	/**
	 * inner class for an undo button
	 */
	public class UndoListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			// undo the last move made
			model.undo();
		}
	}
	
	/**
	 * creates a cheat frame and adds it as an observer to the model
	 */
	public class CheatListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			//model.addObserver(new CheatFrame(model, cardColors));
			ArrayList<CardButton> cheatButtons = new ArrayList<CardButton>();
			ArrayList<CardFace> cheatCards = model.cheat();
			for( int i = 0; i < cheatCards.size(); i++ ){
				CardButton button = new CardButton(i);
				int cardNum = cheatCards.get(i).getNumber();
				button.setText("" + cardNum);
				button.setBackground(cardColors.get(cardNum));
				cheatButtons.add(button);
			}
			
			new CheatFrame(cheatButtons, 4);
		}
	}
}