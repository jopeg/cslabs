/**
 * Redact.java
 * 
 * File:
 * 		$Id: Redact.java,v 1.2 2013/04/29 19:32:31 jdg7026 Exp $
 * Revisions:
 * 		$Log: Redact.java,v $
 * 		Revision 1.2  2013/04/29 19:32:31  jdg7026
 * 		finished
 *
 * 
 */
/**
 * Main class for redacting a given wordlist from a given file
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 */

import java.io.*;
import java.util.*;

public class Redact{	
	
	/**
	 * generates a word list based on the given text file
	 * if an input file is given, opens a file reader for it
	 * if an output file is given opens a file writer for it
	 * 
	 * @param args[] - command line arguments
	 * 		Usage: java Redact word-list input-file output-file
	 */
	public static void main(String[] args){
		ArrayList<String> words = new ArrayList<String>();
		Reader reader;
		Writer writer;
		// command line arguments were wrong
		if ( args.length != 3 ){
			System.out.println( "Usage: java Redact word-list input-file output-file" );
			return;
		} else {
			try{
				// get redact list
				BufferedReader in = 
					new BufferedReader( new FileReader( args[0] ) );
				String word = in.readLine();
				while ( word != null ){
					words.add( word );
					word = in.readLine();
				}
				in.close();
				
				// is the user trying to use system.in?
				if( args[1].equals( "-" ) ){
					reader = 
						new BufferedReader( 
							new InputStreamReader( System.in ) );
				// user is trying to use a file for input
				} else {
					reader = new FileReader( args[1] );
				}
				
				// is the user trying to use system.out?
				if( args[2].equals( "-" ) ){
					writer = 
						new BufferedWriter( 
							new PrintWriter( System.out ) );
				// user is trying to use a file for output
				} else {
					writer = new FileWriter( args[2] );
				}
				
				// create a RedactWriter
				RedactWriter write = new RedactWriter(writer, words);
				
				// go through the reader letter by letter 
				// then pass to the writer
				int letter = reader.read();
				while( letter != -1 ){
					write.write( letter );
					letter = reader.read();
				}
				
				write.close();
				
			} catch( FileNotFoundException e ){
				e.printStackTrace();
			} catch( IOException e ){
				e.printStackTrace();
			} catch( IllegalArgumentException e ){
				e.printStackTrace();
			}
		}
		
	}
}