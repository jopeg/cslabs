/**
 * RedactWriter.java
 * 
 * File:
 * 		$Id: RedactWriter.java,v 1.2 2013/04/29 19:32:31 jdg7026 Exp $
 * Revisions:
 * 		$Log: RedactWriter.java,v $
 * 		Revision 1.2  2013/04/29 19:32:31  jdg7026
 * 		finished
 *
 * 
 */
/**
 * Writer that redacts all words in a given list
 * by replacing them with x's
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 */
import java.io.*;
import java.util.*;

public class RedactWriter extends Writer{
	private Writer writer;
	private Collection<String> words;
	private String buffer;
	/**
	 * constructs a Redact writer with given redact list
	 * 
	 * @param wr - writer to use after characters are checked
	 * @param redacts - collection of words to be redacted
	 * @throws IllegalArgumentException - if any given string in redacts
	 * 		contains a character that is not alpha, or is an empty string
	 */
	public RedactWriter( Writer wr, Collection<String> redacts ) 
						throws IllegalArgumentException{
		this.writer = wr;
		
		for( String word : redacts ){
			if( word == "" ){
				throw new IllegalArgumentException();
			}
			for( char c : word.toCharArray() ){
				if( !Character.isLetter( c ) ){
					throw new IllegalArgumentException();
				}
			}
		}
		
		this.words = redacts;
		this.buffer = "";
	}
	
	/**
	 * writes a given character to the buffer,
	 * until a non alpha char is seen, then 
	 * checks if the buffer is a word to redact
	 * 
	 * @param int c - int representation of a char to write
	 * @throws IOException
	 */
	public void write( int c ) throws IOException{
		char ch = (char)c;
		if( Character.isLetter( ch ) ){
			buffer = buffer + ch;
		} else {
			if ( words.contains(buffer) ){
				for ( int i = 0; i < buffer.length(); i++ )
					writer.write( (int)'X' );
				writer.write( ch );
				buffer = "";
			} else {
				buffer = buffer + ch;
				writer.write( buffer.toCharArray(), 0, buffer.length() );
				buffer = "";
			}
		}
	}
	
	/**
	 * writes a given character array to the buffer,
	 * from the offset to the length given until a 
	 * non alpha char is seen, then checks if the 
	 * buffer is a word to redact
	 * 
	 * @param char[] cbuf - character array of characters to write
	 * @param int off - first index to begin at when writing
	 * @param int len - num of chars to write
	 * @throws IOException
	 */
	public void write( char[] cbuf, int off, int len ) throws IOException{
		for( int i = 0; i < len; i++ ){
			int c = (int)cbuf[off + i];
			this.write( c );
		}
	}
	/**
	 * flush the writer
	 */
	public void flush() throws IOException{
		writer.flush();
	}
	
	/**
	 * close the writer
	 */
	public void close() throws IOException{
		writer.close();
	}
}