/**
 * Snowflake.java
 * 
 * File:
 * 		$Id: Snowflake.java,v 1.1 2013/03/08 22:18:43 jdg7026 Exp $
 * 
 * Revisions:
 * 		$Log: Snowflake.java,v $
 * 		Revision 1.1  2013/03/08 22:18:43  jdg7026
 * 		Snowflake is finished and commented
 *
 * 
 * 		@author Joseph Gambino<jdg7026@rit.edu>
 */

import java.util.Scanner;

public class Snowflake{
	
	/*
	 * initializes the turtle window by setting the coordinate system,
	 * setting the window title, and creating the turtle
	 * 
	 * Parameters
	 * 	int s: length of one segment of the snowflake
	 * 
	 * Returns
	 * 	Turtle: the turtle object used to draw
	 */
	public static Turtle init(int s){
		Turtle t = new Turtle(1.5*s,1.5*s,0);
		t.setCanvasTitle("Snowflake");
		t.setWorldCoordinates(0, 0, 3*s, 3*s);
		return t;
	}
	
	/*
	 * recursively draws one part of a snowflake
	 * 
	 * Parameters
	 * 	int s: length of one segment of snowflake
	 * 	int n: depth of recursion
	 * 	Turtle t: the turtle object to use to draw
	 * 
	 * Returns
	 * 	void
	 */
	public static void snowflake(int s, int n , Turtle t){
		if(n > 0){
			t.goForward(s);
			if(n > 1) {
				t.turnLeft(120);
				for(int i = 0; i < 5; i++){
					snowflake(s/3, n-1, t);
					t.turnRight(60);
				}
				t.turnRight(180);
			}
			t.goForward(-s);
		} 
	}
	
	/*
	 * reads user input, calls init function, then turns the turtle and calls
	 * the snowflake function to draw each section
	 * 
	 * Parameters
	 * 	String[] args: string array containing command line arguments
	 * 
	 * Returns
	 * 	void
	 */
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter S: ");
		int s = scan.nextInt();
		System.out.print("Enter N: ");
		int n = scan.nextInt();
		scan.close();
		
		Turtle t = init(s);
		
		for (int i = 0; i < 6; i++){
			snowflake(s,n,t);
			t.turnLeft(60);
		}
	}
}