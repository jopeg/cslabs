/**
 *PizzaRun.java
 *
 *File:
 *     $Id: PizzaRun.java,v 1.1 2013/03/08 20:05:11 jdg7026 Exp $
 *
 *Revisions:
 *     $Log: PizzaRun.java,v $
 *     Revision 1.1  2013/03/08 20:05:11  jdg7026
 *     Pizza Run is finished and commented
 *
 *     @author Joseph Gambino<jdg7026@rit.edu>
 */

import java.lang.Math;

public class PizzaRun {

        /* Calculates the number of whole pies needed to satisfy a given number of
         * desired slices.
         *
         * Parameters
         *  int nSlices: number of desired slices
         *
         * Returns
         *  int: total number of pies to order
         */
    public static int calcWholePies(int nSlices){
        Integer pies = (int)(Math.ceil(nSlices/8.0));
        return pies;
    }

        /* Prints the number of pies required to satisfy a desired ammount of slices as
         * well as the price of those pies and the extra slices.
         *
         * Parameters
         *  String[] args: a string array containing all of the command line arguments.
         *
         * Returns
         *  void
         */
    public static void main(String[] args) {
	    Double pricePerPie = new Double(args[0]);
        Integer nSlices = 0;

        for(int i = 1; i < args.length; i++){
            nSlices += new Integer(args[i]);
        }

        Integer numPies = calcWholePies(nSlices);
        Double totalPrice = numPies*pricePerPie;
        Integer extraSlices = Math.abs((numPies*8)-nSlices);

        System.out.println("Buy " + numPies + " pizzas for $" + totalPrice);
        System.out.println("There will be " + extraSlices + " extra slices.");
    }
}