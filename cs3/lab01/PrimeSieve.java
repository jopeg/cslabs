/**
 * PrimeSieve.java
 * 
 *File:
 *	$Id: PrimeSieve.java,v 1.2 2013/03/08 21:22:00 jdg7026 Exp $
 *
 *Revisions:
 *	$Log: PrimeSieve.java,v $
 *	Revision 1.2  2013/03/08 21:22:00  jdg7026
 *	changed the comment $id$ in Prime Sieve to $Id$ so that CVS would work properly
 *
 *	Revision 1.1  2013/03/08 21:20:11  jdg7026
 *	Prime Sieve is finished and commented
 *
 *
 *	@author Joseph Gambino<jdg7026@rit.edu>
 */

import java.util.ArrayList;
import java.util.Scanner;
import java.lang.Math;

public class PrimeSieve{
	
	/* Find and return the prime numbers between 2 and N inclusive.
	 *
	 * Parameters
	 *  int N: the maximum value to check (inclusive)
	 *
	 * Returns
	 *  ArrayList<Integer>: contains all the prime numbers from 2 to N 
	 */
	public static ArrayList<Integer> findPrimes(int N){
		ArrayList<Boolean> marked = new ArrayList<Boolean>();
		ArrayList<Integer> primes = new ArrayList<Integer>();
		for(int i = 0; i <= N; i++){
			marked.add(true);
		}
		for(int j = 2; j <= Math.sqrt(N); j++){
			if(marked.get(j)){
				for(int k = j*j; k <= N; k += j){
					marked.set(k,false);
				}
			}
		}
		for(int l = 2; l <= N; l++){
			if(marked.get(l)){
				primes.add(l);
			}
		}
		return primes;
	}
	
	/* Prints the prime numbers between 2 and the value N given by the user
	 * 
	 * Parameters
	 *  String[] args: a string array containing the command line arguments
	 *  
	 * Returns
	 *  void
	 */
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("Compute prime numbers from 2 to: ");
		Integer N = scan.nextInt();
		scan.close();
		if(N <= 2){
			System.out.println("N must be greater than or equal to 2.");
		} else {
			ArrayList<Integer> primes = findPrimes(N);
			System.out.print("Prime numbers: ");
			for(int i: primes){
				System.out.print(i + " ");
			}
		}
	}
}