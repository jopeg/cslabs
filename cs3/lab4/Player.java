/*
 * Player.java
 *
 * Version:
 * $Id: Player.java,v 1.1 2013/04/09 04:43:33 jdg7026 Exp $
 *
 * Revisions:
 * $Log: Player.java,v $
 * Revision 1.1  2013/04/09 04:43:33  jdg7026
 * finished the whole lab.
 *
 */

/**
 * A player abstract class for poker
 *
 * @author Joseph Gambino <jdg7026@rit.edu>
 */

public abstract class Player{
	public int numWins;
	public PokerHand myCards;
	public abstract boolean stand();
	public boolean standing;
	public String playerType;
	public int playerId;
	/**
	 * Adds card to the players hand
	 *
	 * @param Card c = what card to add
	 */
	public void addCard(Card c){
		myCards.addCard(c);
	}

	/**
	 * prints the hand in some "nice" format
	 */
	public void printHand(){
		myCards.printHand();
		System.out.println();
		System.out.println( "Total:" + value() + "\n" );
	}

	/**
	 * gives the players a new PokerHand
	 */
	public void newHand(){
		myCards = new PokerHand();
	}
	/**
	 * returns the value of the players hand.
	 *
	 * @return int, the value of the hand
	 */
    public int value() {
		return myCards.value();
    }
	/**
	 * compares one Player's hand value to another players
	 * hand value
	 * 
	 * @return int, positive if players hand is better
	 * 				negative if players hand is worse
	 * 				and 0 if players hands are equal
	 */
	public int compareTo(Player d){
		int res;
		if( myCards.value() > d.value() ){
			res = 1;
		} else if( myCards.value() == d.value() ){
			res = 0;
		} else {
			res = -1;
		}
		return res;
	}
}
