/*
 * Poker.java
 *
 * Version:
 * $Id: Poker.java,v 1.1 2013/04/09 04:43:34 jdg7026 Exp $
 *
 * Revisions:
 * $Log: Poker.java,v $
 * Revision 1.1  2013/04/09 04:43:34  jdg7026
 * finished the whole lab.
 *
 */

import java.util.Scanner;
import java.util.ArrayList;

/**
 * A poker game played between human and computer players
 *
 * @author paw: Phil White
 * @author Joseph Gambino <jdg7026@rit.edu>
 */

public class Poker {

   /**
	* An integer stating which player goes first this hand.
	* Incremented by one after each hand, loops back to 0
	* after last player goes first
    */
	/**
	 * An array list to hold the players in the game
	 */
	private static ArrayList<Player> players = new ArrayList<Player>();
   /**
    * Plays a single hand of poker
    *
    * @param    ArrrayList<Player> players = array of all players in game
    * @param    d	The deck
    * @return   An int telling the user who won (their index in the array)
    */

	public static void shufflePlayers(){
		Player tempPlayer = players.get(0);
		players.remove(tempPlayer);
		players.add(tempPlayer);
	}

    public static int playHand( ArrayList<Player> players, Deck d ){
		Player winner;
		Player player;
		boolean standing = true;
		int ties = 1;

		System.out.println( "== Dealing Cards\n" );
		//give initial cards 
		for (Player playerNow : players){
			playerNow.addCard(d.dealCard());
			playerNow.addCard(d.dealCard());
		}
		
		
		//print cards and see who wants to stand/fold
		for (int i = 0; i < players.size(); i++){
			player = players.get(i);
			if (i == players.size()-1 && !standing){
				player.standing = true;
				System.out.println(player.playerType + player.playerId 
						+ " is the only player left, so they "
						+ " stand automatically");
			} else {
				if (player instanceof Human){
					System.out.println("\n ==== " + player.playerType 
							+ player.playerId + "'s Cards ==== ");
					player.standing = player.stand();
				} else {
					player.standing = player.stand();
					System.out.println("\n Computer " 
							+ player.playerType + player.playerId 
							+ ((player.standing)? " Stands":" Folds"));
				}
				if(player.standing){ standing = true; }
			}
		}

		System.out.println(" ==== Results ==== ");
		// of the players standing, sort them by best hand
		// and print their hand to show results
		winner = players.get(0);
		for (int j = 0; j < players.size(); j++){
			player = players.get(j);
			if ( j != 0 ){
				if (player.standing){
					int better = player.compareTo(winner);
					if (better == 1) { 
						winner = player;
						ties = 1;
					}
					else if (better == 0) { 
						ties++;
					}
				}
			}
			if (player.standing){
				System.out.println(" ---- " + player.playerType
						+ player.playerId + "'s Cards ---- ");
				player.printHand();
			}
		}
		// clear all players hands
		for (Player playerNow : players){
			playerNow.newHand();
		}
		if (ties <= 1){
			return winner.playerId;
		} else {
			return -ties;
    	}
	}
   /**
    * main method -- plays multiple hands of poker, after each hand
    * ask the user if they wan0t to play again.  We also keep trak of
    * the number of games played/won by each player 
	* and print the results at the end.
    *
    * @param    args      command line arguments
    */
    public static void main( String args[] ){
		Scanner in = new Scanner( System.in );
		String again;
		String isHuman;
		String playerType;
		char c;
		char p;
		int num;
		int winner;
		int numGames = 0;
		int numTie = 0;

		Player winPlayer;
		Player currPlayer;
		Player creating;

		Deck theDeck = new Deck();

		System.out.println("How many players for this poker game: ");
		num = in.nextInt();
		
		for (int i = 0; i < num; i++){
			do{
				System.out.println("Is player " + i 
						+ " a human or a computer (h/c): ");
				isHuman	= in.next();
				isHuman = isHuman.toLowerCase();
				p = isHuman.charAt( 0 );
			}while(p != 'c' && p != 'h');
			if (p == 'c'){ 
				creating = new Computer(i);
				players.add(creating);
			} else if (p == 'h'){ 
				creating = new Human(in,i);
				players.add(creating);
			}
		}
		do {
			numGames = numGames + 1;
			
	    	System.out.println();
	    	System.out.println( "##########################################" );
	    	System.out.println( "##########       NEW HAND      ###########" );
	    	System.out.println( "##########################################" );
	    	System.out.println( "\n== Shuffling" );
	    	theDeck.shuffle();

	    	winner = playHand( players, theDeck );
			
			if (winner > -1){
	    		winPlayer = players.get(winner);
				winPlayer.numWins++;
				System.out.println("\n **** " + winPlayer.playerType 
						+ winPlayer.playerId + " Won! **** \n");
			} else {
				winner = -winner;
				System.out.println( "\n ---- " + winner 
						+ "Players Tied ---- \n" );
				numTie++;
			}

	    do{
	    	System.out.print( "Do you wish to play " +
				    "another hand (y/n):" );
		again = in.next();
		again = again.toLowerCase();
		c = again.charAt( 0 );
	    }while( c != 'y' && c != 'n' );

		shufflePlayers();
	} while ( c == 'y' );
		
	System.out.println( "========== Results ==========" );
	System.out.println( "Games Played: " + numGames );
	for (int k = 0; k < players.size(); k++){
		currPlayer = players.get(k);
		System.out.println( currPlayer.playerType + k 
				+ " Won: " + currPlayer.numWins); 
	}
	System.out.println( "Ties: " + numTie );

	in.close();
    }
} 
