/*
 * Computer.java
 *
 * Version:
 * $Id: Computer.java,v 1.1 2013/04/09 04:43:33 jdg7026 Exp $
 *
 * Revisions:
 * $Log: Computer.java,v $
 * Revision 1.1  2013/04/09 04:43:33  jdg7026
 * finished the whole lab.
 *
 */

/**
 * A computer player for poker
 *
 * @author Joseph Gambino <jdg7026@rit.edu> 
 */

public class Computer extends Player {
    private static final int BETTER_THAN_HALF_WIN_VALUE =
		( Ranks.QUEEN.getValue() ) * 14 + 
		( Ranks.JACK.getValue() );

   /**
    * Initalize a computer player for poker
	* @param int i = player Id
    */
    public Computer (int i){
		playerId = i;
		playerType = "Computer #";
		numWins = 0;
		myCards = new PokerHand();
    }

   /**
    * determines if the computer should stand (vs fold).  Note the
    * computer will stand if it has >=50% chance of winning (Based on
    * other work, a High Card hand with a Q and J beats 50% of the other
    * possible hands).  For the complete odds of winning see 
<a href="../chance.html">chance.html</a> for tables containing the chance 
    * to win for 2-cards of the same suit, and 2 cards of unmatched suits
    *
    * @return	a boolean value specifying if the computer wants to stand
    */
    public boolean stand(){
		return ( myCards.value() >= BETTER_THAN_HALF_WIN_VALUE );
    }

   /**
    * main method for a test driver that should test all the methods
    * in the class
    *
    * @param    args    command line arguments
    */
    public static void main( String args[] ){
    }
}
