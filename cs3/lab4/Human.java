/*
 * Human.java
 *
 * Version:
 * $Id: Human.java,v 1.1 2013/04/09 04:43:33 jdg7026 Exp $
 *
 * Revisions:
 * $Log: Human.java,v $
 * Revision 1.1  2013/04/09 04:43:33  jdg7026
 * finished the whole lab.
 *
 */

/**
 * A Human class for poker
 *
 * @author Joseph Gambino <jdg7026@rit.edu>
 */
import java.util.Scanner;
public class Human extends Player{
    private static Scanner in;
   /**
    * Initialize a human player for poker
	*
	* @param Scanner inScan = scanner for the game
	* @param int i = playerId
    */
    public Human ( Scanner inScan, int i){
		playerId = i;
		playerType = "Human #";
		numWins = 0;
		in = inScan;
		myCards = new PokerHand();
    }

   /**
    * Asks the player if they want to stand.  You should prompt the
    * player with a suitable message, and then read the players response
    * from standard input.
    *
    * @return  a boolean value specifying if the human wants to stand
    */
    public boolean stand(){
		boolean res = true;
		String answer;
		char c;
		
		printHand();
			do{
				System.out.print("Do you want to stand (y/n)? ");
				answer = in.next();
			answer = answer.toLowerCase();
			c = answer.charAt(0);
		}while( c != 'y' && c != 'n' );

		if ( c == 'y' ){
			res = true;
		} else {
		    res = false;
		}
		return res;
    }

   /**
    * main method for a test driver that should test all the methods
    * in the class
    *
    * @param    args    command line arguments
    */
    public static void main( String args[] ){
    }
}
