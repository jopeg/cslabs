/**
 * MulExpression.java
 * 
 * File:
 * 		$Id: MulExpression.java,v 1.1 2013/03/19 21:08:05 jdg7026 Exp $
 * 
 * Revisions:
 * 		$Log: MulExpression.java,v $
 * 		Revision 1.1  2013/03/19 21:08:05  jdg7026
 * 		finished
 *
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 * 
 */
public class MulExpression implements Expression {
	private Expression left;
	private Expression right;

	/**
	 * constructs a multiplication expression with a left and right expression
	 * 
	 * @param exp1 = left expression object for the node
	 * @param exp2 = right expression object for the node
	 */
	public MulExpression(Expression exp1, Expression exp2){
		this.left = exp1;
		this.right = exp2;
	}

	/**
	 * @return integer value of the node after evaluated
	 */
	public Integer evaluate(){
		return left.evaluate() * right.evaluate();
	}

	/**
	 * @returns string of the infix notation for the node
	 */
	public String emit(){
		return "( " + left.emit() + " * " + right.emit() + " )";
	}
}