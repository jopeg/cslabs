/**
 * Expression.java
 * 
 * File:
 * 		$Id: Expression.java,v 1.1 2013/03/19 21:08:03 jdg7026 Exp $
 * 
 * Revisions:
 * 		$Log: Expression.java,v $
 * 		Revision 1.1  2013/03/19 21:08:03  jdg7026
 * 		finished
 *
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 * 
 */
public interface Expression{
	Integer evaluate();
	String emit();
}