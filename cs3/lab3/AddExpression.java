/**
 * AddExpression.java
 * 
 * File:
 * 		$Id: AddExpression.java,v 1.1 2013/03/19 21:08:03 jdg7026 Exp $
 * 
 * Revisions:
 * 		$Log: AddExpression.java,v $
 * 		Revision 1.1  2013/03/19 21:08:03  jdg7026
 * 		finished
 *
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 * 
 */
public class AddExpression implements Expression {
	private Expression left;
	private Expression right;

	/**
	 * constructs an addition expression with a left and right expression
	 * 
	 * @param exp1 = left expression object for the node
	 * @param exp2 = right expression object for the node
	 */
	public AddExpression(Expression exp1, Expression exp2){
		this.left = exp1;
		this.right = exp2;
	}

	/**
	 * @return integer value of the node after evaluated
	 */
	public Integer evaluate(){
		return left.evaluate() + right.evaluate();
	}

	/**
	 * @returns string of the infix notation for the node
	 */
	public String emit(){
		return "( " + left.emit() + " + " + right.emit() + " )";
	}
}