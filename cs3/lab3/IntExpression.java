/**
 * IntExpression.java
 * 
 * File:
 * 		$Id: IntExpression.java,v 1.1 2013/03/19 21:08:06 jdg7026 Exp $
 * 
 * Revisions:
 * 		$Log: IntExpression.java,v $
 * 		Revision 1.1  2013/03/19 21:08:06  jdg7026
 * 		finished
 *
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 * 
 */
public class IntExpression implements Expression {
	private Integer value;

	/**
	 * constructs an Integer expression with value being the 
	 * given string as an integer
	 * 
	 * @param num = string representing the integer value to be given
	 */
	public IntExpression(String num){
		this.value = Integer.parseInt(num);
	}

	/**
	 * returns value
	 * 
	 * @return integer value
	 */
	public Integer evaluate(){
		return this.value;
	}

	/**
	 * returns the string representation of value
	 * 
	 * @return string value
	 */
	public String emit(){
		return this.value.toString();
	}
}