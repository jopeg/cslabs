/**
 * Parse.java
 * 
 * File:
 * 		$Id: Parse.java,v 1.2 2013/03/19 22:20:24 jdg7026 Exp $
 * 
 * Revisions:
 * 		$Log: Parse.java,v $
 * 		Revision 1.2  2013/03/19 22:20:24  jdg7026
 * 		changed parse a little to make it cleaner
 *
 * 		Revision 1.1  2013/03/19 21:08:06  jdg7026
 * 		finished
 *
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 * 
 */
import java.util.ArrayList;
import java.util.Arrays;

public class Parse {
	/**
	 * takes an input string, splits it into an array list, then calls the recursive
	 * parse function.
	 * 
	 * @param s = string containing the expression to evaluate in prefix notation.
	 * @return an Expression object containing the tree for the parsed string
	 */
	public static Expression parseString(String s){
		ArrayList<String> sList = new ArrayList<String>(Arrays.asList(s.split(" ")));
		return parse(sList);
	}

	/**
	 * recursively creates an Expression tree for the given array list of strings
	 * 
	 * @param sList = array list containing strings for each item
	 * @return Expression tree
	 */
	private static Expression parse(ArrayList<String> sList){
		if (sList.get(0).equals("+")){
			sList.remove(0);
			Expression e = new AddExpression(parse(sList), parse(sList));
			return e;
		} else if (sList.get(0).equals("-")) {
			sList.remove(0);
			Expression e = new SubExpression(parse(sList), parse(sList));
			return e;
		} else if (sList.get(0).equals("*")) {
			sList.remove(0);
			Expression e = new MulExpression(parse(sList), parse(sList));
			return e;
		} else {
			Expression e = new IntExpression(sList.get(0));
			sList.remove(0);
			return e;
		}
	}
}