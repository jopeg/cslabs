/**
 * SubExpression.java
 * 
 * File:
 * 		$Id: SubExpression.java,v 1.1 2013/03/19 21:08:04 jdg7026 Exp $
 * 
 * Revisions:
 * 		$Log: SubExpression.java,v $
 * 		Revision 1.1  2013/03/19 21:08:04  jdg7026
 * 		finished
 *
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 * 
 */
public class SubExpression implements Expression {
	private Expression left;
	private Expression right;

	public SubExpression(Expression exp1, Expression exp2){
		this.left = exp1;
		this.right = exp2;
	}

	/**
	 * @return integer value of the node after evaluated
	 */
	public Integer evaluate(){
		return left.evaluate() - right.evaluate();
	}

	/**
	 * @returns string of the infix notation for the node
	 */
	public String emit(){
		return "( " + left.emit() + " - " + right.emit() + " )";
	}
}