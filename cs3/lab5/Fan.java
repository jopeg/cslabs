/**
 * File:
 *   $Id: Fan.java,v 1.2 2013/04/16 14:14:05 jdg7026 Exp $
 *   
 * Revisions:
 *   $Log: Fan.java,v $
 *   Revision 1.2  2013/04/16 14:14:05  jdg7026
 *   finished
 *
 *   Revision 1.1  2013/04/16 13:33:55  jdg7026
 *   finished, time to comment
 *
 */

import java.util.*;

/**
 * Fan implements the Fan class
 * @author Aaron T Deever
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */
public class Fan {

    public static final int FAN_MONEY = 50;
    private int money;
    private Dealer dealer;
    private Scanner in = new Scanner(System.in);
    private ArrayList<BaseballCard> myCards = 
                new ArrayList<BaseballCard>();

    
    
    public Fan(){
        this.money = FAN_MONEY;
        this.dealer = new Dealer();
    }
    
    /**
     * Main method for Baseball card simulation
     * @param args not used
     */
    
    public static void main(String[] args) {
        
        if(args.length != 0) { 
            System.out.println("Usage: java Fan");
            return;
        }
        
        Fan fan = new Fan();
        fan.simulate();
    }
    
    /**
     * Method to simulate the interaction of a Fan and Dealer to buy/trade cards
     */
    public void simulate() { 
        
        boolean quit = false;
        
        do { 
            System.out.println();
            System.out.print("Options: buy (P)ack / buy (C)ard / (T)rade");
            System.out.println(" / (S)tatus / (Q)uit");
            System.out.print("Command: ");
            
            if(in.hasNext()) { 
                String cmd = in.next();
                switch(cmd.charAt(0)) { 
                case 'P':
                case 'p':
                    purchasePack();
                    break;
                case 'C':
                case 'c':
                    purchaseCard();
                    break;
                case 'T':
                case 't':
                    makeTrade();
                    break;
                case 'S':
                case 's':
                    status();
                    break;
                case 'Q':
                case 'q':
                    quit = true;
                    break;
                default:
                    break;
                }
            }
            else { 
                return;
            }
            
        } while (!quit);
    }
    
    /**
     * Method to simulate a trade between Fan and Dealer. 
     */
    public void makeTrade(){
        System.out.print("Fan Player: ");
        String fanPlayer = in.next();
        System.out.print("Dealer Player: ");
        String dealerPlayer = in.next();
        for (BaseballCard card : myCards){
            if (card.getPlayerName().equals(fanPlayer)){
                BaseballCard traded = 
                    dealer.trade(fanPlayer, dealerPlayer);
                if (traded != null){
                    myCards.add(traded);
                    myCards.remove(card);
                    System.out.println("Trade Completed");
                } else { System.out.println("Trade Unsucessful, Dealer refused."); }
                break;
            }
        }
    }
    /**
     * Method to purchase one card from the dealer.
     */
    public void purchaseCard(){
        System.out.print("Card to Purchase: ");
        String player = in.next();
        // is the card a real card?
        for (BaseballCard card : dealer.getCompleteSet()){
            if (card.getPlayerName().equals(player)){
                // do I have the money?
                if (this.money > dealer.COST_PER_CARD){
                    BaseballCard card2 = dealer.buyCard(player);
                    myCards.add(card2);
                    money = money-dealer.COST_PER_CARD;
                    System.out.println("Purchase succesful");
                    break;
                } else { 
                    System.out.println("Purchase unsuccessful, not enough funds"); 
                    break;
                } 
            }
        }
    }
    /**
     * Method to purchase a pack from the dealer.
     */
    public void purchasePack(){
        // do I have the money?
        if (money > dealer.COST_PER_PACK){
            Collection<BaseballCard> pack = dealer.buyPack();
            myCards.addAll(pack);
            money = money-dealer.COST_PER_PACK;
            System.out.println("Purchase successful");
        } else {
            System.out.println("Purchase unsuccessful, not enough funds");
        }
    }
    /**
     * Prints the status of the simulation, showing how much money the fan has
     * what cards the fan has, what they need, and what cards they have extra.
     */
    public void status(){
        Collections.sort(myCards);
        //cards fan has
        HashSet<BaseballCard> haves = new HashSet<BaseballCard>(myCards);
        //full set of cards
        Collection<BaseballCard> completeSet = dealer.getCompleteSet();
        //cards the fan doesn't have
        ArrayList<BaseballCard> havenots = new ArrayList<BaseballCard>();
        //extra cards
        ArrayList<BaseballCard> extras = new ArrayList<BaseballCard>(myCards);
        //remove one of each card the fan has from the extras
        //because those are the cards the fan has that aren't
        //extra
        for (BaseballCard card : haves){
            extras.remove(card);
        }
        //if the fan doesn't have the card, add it to what the fan needs
        for (BaseballCard havenot : completeSet){
            if (!myCards.contains(havenot)){ havenots.add(havenot); }
        }

        System.out.println("Fan has $" + money + " remaining.");
        System.out.println("Fan has: " + haves);
        System.out.println("Fan needs: " + havenots);
        System.out.println("Fan has extra: " + extras);
    }

}
