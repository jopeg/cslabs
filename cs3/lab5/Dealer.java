/**
 * File:
 *   $Id: Dealer.java,v 1.2 2013/04/16 14:14:05 jdg7026 Exp $
 *   
 * Revisions:
 *   $Log: Dealer.java,v $
 *   Revision 1.2  2013/04/16 14:14:05  jdg7026
 *   finished
 *
 *   Revision 1.1  2013/04/16 13:33:55  jdg7026
 *   finished, time to comment
 *
 */

import java.util.*;

/**
 * Dealer implements the Dealer class
 * @author Aaron T Deever
 * @author Joseph Gambino <jdg7026@rit.edu>
 */
public class Dealer {
    
    /**
     * Dealer stores information about the baseball cards contained in this
     * special edition.
     */

    public final int CARDS_PER_PACK = 5;
    public final int COST_PER_PACK = 10;
    public final int COST_PER_CARD = 3;

    private HashMap<String, Integer> playerData = 
            new HashMap<String, Integer>();
    
    /**
     * Constructor
     */
    public Dealer() { 
        playerData.put("Bonds", 762);
        playerData.put("Aaron",  755);
        playerData.put("Ruth", 714);
        playerData.put("Mays",  660);
        playerData.put("Rodriguez",  647);
        playerData.put("Griffey", 630);
        playerData.put("Thome",  612);
        playerData.put("Sosa", 609);
        playerData.put("Robinson", 586);
        playerData.put("McGwire", 583);
    }
    
    /**
     * Method to create a collection of the entire special edition of cards 
     * that can be used as a basis of comparison 
     * @return Collection<BaseballCard> containing every card in the special
     *          edition.
     */
    public Collection<BaseballCard> getCompleteSet(){
        ArrayList<BaseballCard> completeSet = 
            new ArrayList<BaseballCard>();
        for(String name : playerData.keySet()){
            completeSet.add(new BaseballCard(name, playerData.get(name)));
        }
        return completeSet;
    }

    /**
     * Creates a pack of random new cards for the player
     * @return Collection<BaseballCard> containing the cards in the pack
     */
    public Collection<BaseballCard> buyPack(){
        ArrayList<BaseballCard> pack = 
            new ArrayList<BaseballCard>();
        Random rand = new Random();
        ArrayList<BaseballCard> completeSet = 
            new ArrayList<BaseballCard>(getCompleteSet());
        do{ 
            int num = rand.nextInt(10);
            if (!(pack.contains(completeSet.get(num)))){
                pack.add(completeSet.get(num));
            }
        } while (pack.size() != CARDS_PER_PACK);
        return pack;
    }

    /**
     * creates a new baseball card for the player name provided
     * @param String player player name
     * @return BaseballCard a card of the player whos name was given
     */
    public BaseballCard buyCard(String player){
        return new BaseballCard(player, playerData.get(player));
    }
    
    /**
     * Checks if fan card is more valuable then dealer card, then
     * and either trades it or returns null
     * @param fanPlayer player for the fan to trade
     * @param dealerPlayer player for the dealer to trade
     * @return BaseballCard for the dealer player if the dealer accepts
     *          the trade, else returns null.
     */
    public BaseballCard trade(String fanPlayer, String dealerPlayer){
        if (playerData.containsKey(fanPlayer)&& 
                playerData.containsKey(dealerPlayer)){
            BaseballCard fanCard = 
                    new BaseballCard(fanPlayer, playerData.get(fanPlayer));
            BaseballCard dealerCard = 
                    new BaseballCard(dealerPlayer, playerData.get(dealerPlayer));
            if (fanCard.compareTo(dealerCard) > 0){ return dealerCard; }
        } 
        return null;
    }

    

}
