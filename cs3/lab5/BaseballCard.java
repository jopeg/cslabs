/*
 * BaseballCard.java
 *
 * File:
 *  $Id: BaseballCard.java,v 1.2 2013/04/16 14:14:06 jdg7026 Exp $
 *
 * Revisions:
 *  $Log: BaseballCard.java,v $
 *  Revision 1.2  2013/04/16 14:14:06  jdg7026
 *  finished
 *
 *  Revision 1.1  2013/04/16 13:33:56  jdg7026
 *  finished, time to comment
 *
 */
/**
 * BaseballCard implements the BaseballCard class
 */
public class BaseballCard implements Comparable<BaseballCard>{
    private String name;
    private int homers;

    /**
     * creates a new baseball card that is the same as the given one
     * @param BaseballCard bc a baseball card to make a copy of
     */
    BaseballCard(BaseballCard bc){
        this.name = bc.getPlayerName();
        this.homers = bc.getHomeRuns();
    }
    
    /**
     * creates a new baseball card with name and homeruns given
     * @param String name player name for the card
     * @param int homers player homeruns for card
     */
    BaseballCard(String name, int homers){
        this.name = name;
        this.homers = homers;
    }
    
    /**
     * implements comparable, allows for comparing of two baseball cards
     * @return int 1 if this is greater, 
     *             0 if they're equal, 
     *             -1 if this is less than
     */
    public int compareTo(BaseballCard bc){
        if (this.homers == bc.getHomeRuns()){
            return this.name.compareTo(bc.getPlayerName());
        } else { return ((Integer)this.homers).compareTo(bc.getHomeRuns()); }
    }

    /**
     * Returns string representation of the card
     * @return String playerName:homeRuns
     */
    public String toString(){
        return this.name + ":" + this.homers;
    }

    /**
     * implements comparable
     * @return boolean true if objects are equal, false otherwise.
     */
    public boolean equals(Object o){
        if (!(o instanceof BaseballCard)){
            return false;
        } else { return (this.compareTo((BaseballCard)o) == 0); }
    }

    /**
     * generates a hash code for the card using the string hashcode
     * function on the string representation of the card.
     * @return int the hash code
     */
    public int hashCode(){
        return this.toString().hashCode();
    }
    
    /**
     * @return int number of home runs player has
     */
    public int getHomeRuns(){
        return this.homers;
    }
    
    /**
     * @return String the player name
     */
    public String getPlayerName(){
        return this.name;
    }
}