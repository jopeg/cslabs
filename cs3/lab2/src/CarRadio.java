/**
 * CarRadio.java
 * 
 * File:
 * 		$Id: CarRadio.java,v 1.4 2013/03/19 03:36:25 jdg7026 Exp $
 * 
 * Revisions:
 * 		$Log: CarRadio.java,v $
 * 		Revision 1.4  2013/03/19 03:36:25  jdg7026
 * 		100% done
 *
 * 		Revision 1.3  2013/03/18 19:46:54  jdg7026
 * 		added most docstrings
 *
 * 		Revision 1.2  2013/03/18 19:17:20  jdg7026
 * 		fixed a small error in tune up/down
 *
 * 		Revision 1.1  2013/03/18 19:00:40  jdg7026
 * 		program is fully functional but un-commented
 *
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 * 
 */

import java.util.ArrayList;

public class CarRadio{
	public static final int MAX_VOLUME = 20;
	public static final int MIN_VOLUME = 0;
	private final int[] MAX_FREQ = {FreqBand.FM.maxFreq(), FreqBand.AM.maxFreq()};
	private final int[] MIN_FREQ = {FreqBand.FM.minFreq(), FreqBand.AM.minFreq()};
	private final int[] FREQ_CHANGE = {FreqBand.FM.spacing(), FreqBand.AM.spacing()};

	private int volume = MIN_VOLUME;
	// 0 = not setting 1 = primary setting 2 = secondary setting
	private int freqSet = 0;

	//[FM,AM]
	private int[] frequency = {FreqBand.FM.minFreq(), FreqBand.AM.minFreq()};
	private int[][][] presets = new int[2][5][2];

	private boolean muted = false;
	//true for AM, false for FM
	private boolean amfm = true;
	private boolean power = false;

	private StationData stationData;
	
	
	/**
	 * Construct a car radio with factory default settings and with supplied station data.
	 * 
	 * @param stationData - the local station data, used to determine viable stations and station ids.
	 */
	public CarRadio(StationData stationData){
		this.stationData = stationData;
		// initialize presets
		for (int[][] band : presets) {
			for (int[] freq : band) {
				  freq[0] = MIN_FREQ[0];
				  freq[1] = MIN_FREQ[1];
			}
		}
	}

	/**
	 * turns the radio on or off
	 */
	public void powerBtn(){
		if (this.power) {
			this.power = false;
		} else {
			this.power = true;
		}
		this.freqSet = 0;
	}

	/**
	 * turns the volume up 1, if at max volume nothing happens
	 */
	public void volumeUpBtn(){
		if (this.power) {
			if (this.volume >= MAX_VOLUME){
				this.volume = MAX_VOLUME;
			} else {
				this.volume++;
			}
		}
	}

	/**
	 * turns the volume down 1, if at min volume nothing happens
	 */
	public void volumeDownBtn(){
		if (this.power) {
			if (this.volume <= MIN_VOLUME){
				this.volume = MIN_VOLUME;
			} else {
				this.volume--;
			}
		}
	}

	/**
	 * toggles mute on or off
	 */
	public void muteBtn(){
		if (this.power){
			this.muted = !this.muted;
		}
	}

	/**
	 * toggles between am/fm
	 */
	public void amfmBtn(){
		if (this.power){
			this.amfm = !this.amfm;
			this.freqSet = 0;
		}
	}

	/**
	 * tunes the radio up based on what band the radio is on,
	 * wraps the frequency to the minimum frequency in the band
	 * if the frequency is already at max
	 */
	public void tuneUpBtn(){
		int temp = (this.amfm)? 1:0;
		if (this.frequency[temp] >= this.MAX_FREQ[temp]) {
			this.frequency[temp] -= (this.MAX_FREQ[temp]-this.MIN_FREQ[temp]);
		} else {
			this.frequency[temp] += this.FREQ_CHANGE[temp];
		}
		this.freqSet = 0;
	}

	/**
	 * tunes the radio down based on what band the radio is on,
	 * wraps the frequency to the maximum frequency in the band
	 * if the frequency is already at min
	 */
	public void tuneDownBtn(){
		if (this.power){
			int temp = (this.amfm)? 1:0;
			if (this.frequency[temp] <= this.MIN_FREQ[temp]) {
				this.frequency[temp] += (this.MAX_FREQ[temp]-this.MIN_FREQ[temp]);
			} else {
				this.frequency[temp] -= this.FREQ_CHANGE[temp];
			}
			this.freqSet = 0;
		}
	}

	/**
	 * tunes the radio up until it finds a station that is
	 * broadcasting based on the given stationData
	 */
	public void seekUpBtn(){
		if (this.power){
			int temp = (this.amfm)? 1:0;
			boolean valid = false;
			int lastFreq = this.frequency[temp];
			while (!valid) {
				this.tuneUpBtn();
				if (frequency[temp] != lastFreq) {
					valid = !(this.stationData.lookupFreq(( (this.amfm)? FreqBand.AM:FreqBand.FM ), frequency[temp]) == null);
				} else { valid = true; }
			}
		}
	}

	/**
	 * tunes the radio down until it finds a station that is
	 * broadcasting based on the given stationData
	 */
	public void seekDownBtn(){
		if (this.power){
			int temp = (this.amfm)? 1:0;
			boolean valid = false;
			int lastFreq = this.frequency[temp];
			while (!valid) {
				this.tuneDownBtn();
				if (frequency[temp] != lastFreq) {
					valid = !(this.stationData.lookupFreq(( (this.amfm)? FreqBand.AM:FreqBand.FM ), frequency[temp]) == null);
				} else { valid = true; }
			}
		}
	}

	/**
	 * prepares the radio to set a primary or secondary preset
	 * unprepared ==> prepared for primary ==> prepared for secondary ==> back to unprepared
	 */
	public void setBtn(){
		if (this.power){
			if (this.freqSet == 2) {
				this.freqSet = 0;
			} else {
				this.freqSet++;
			}
		}
	}

	/**
	 * if the radio is unprepared to set a preset:
	 * 		if the radio is not tuned to primary preset:
	 * 			tune to secondary preset button buttonNum
	 * 		else:
	 * 			tunes to primary preset button buttonNum
	 * 
	 * if the radio is prepared to set primary preset:
	 * 		sets primary preset for the button buttonNum to the current station
	 * 
	 * if the radio is prepared to set secondary preset:
	 * 		sets secondary preset for the button buttonNum to the current station
	 * 
	 * @param buttonNum - button number of the preset that was pressed
	 */
	private void preset(int buttonNum){
		if (this.power){
			int temp = (this.amfm)? 1:0;
			if (this.freqSet == 0) {
				if (this.frequency[temp] != this.presets[temp][buttonNum-1][0]){
					this.frequency[temp] = this.presets[temp][buttonNum-1][0];	
				} else {
					this.frequency[temp] = this.presets[temp][buttonNum-1][1];
				}
			} else if (this.freqSet == 1) {
				this.presets[temp][buttonNum-1][0] = this.frequency[temp];
			} else {
				this.presets[temp][buttonNum-1][1] = this.frequency[temp];
			}
			this.freqSet = 0;
		}
	}

	public void preset1Btn(){
		preset(1);
	}

	public void preset2Btn(){
		preset(2);
	}

	public void preset3Btn(){
		preset(3);
	}

	public void preset4Btn(){
		preset(4);
	}

	public void preset5Btn(){
		preset(5);
	}

	/**
	 * creates the lines to represent a screen for a car radio
	 * 
	 * The format of the display is as follows:
	 * 	If the power is off, then:
	 * 		Line 1: A string of 21 '-' characters.
	 * 		Lines 2 and 3: A string of 21 characters, comprised of a '|' character, 
	 * 				19 spaces, and a '|' character.
	 * 		Line 4: A string of 21 '-' characters.
	 * 	If the power is on, then:
	 * 		Line 1: A string of 21 '-' characters.
	 * 		Line 2: A string of 21 characters, comprised of a '|' character, two spaces, 
	 * 				either the string "AM" or the string "FM" (reflecting the current 
	 * 				frequency band), sufficient spaces to right justify the current frequency 
	 * 				in column 12, the tuned frequency, two spaces, the station id if tuned to
	 * 				a viable station or the string "****" if not tuned to a viable station, two 
	 * 				spaces, and a '|' character.
	 * 		Line 3: A string of 21 characters, comprised of a '|' character, two spaces, the 
	 * 				string "Vol:", sufficient spaces to right justify the current volume in 
	 * 				column 10, the current volume if not muted and the string "--" if muted, four 
	 * 				spaces, either four spaces if the radio is unprepared to set a preset frequency
	 * 				or the string "SET1" if the radio is prepared to set a primary preset frequency 
	 * 				or thes string "SET2" if the radio is prepared to set a secondary preset 
	 * 				frequency, two spaces, and a '|' character.
	 * 		Line 4: A string of 21 '-' characters.
	 * 
	 * @return - ArrayList<String> contains 4 strings representing each line of 
	 * 			 the screen
	 */
	public ArrayList<String> display(){
		ArrayList<String> lines = new ArrayList<String>();
		int temp = (this.amfm)? 1:0;
		if (!this.power) {
			lines.add("---------------------");
			lines.add("|                   |");
			lines.add("|                   |");
			lines.add("---------------------");
		} else {
			lines.add("---------------------");
			String tempStr = "|  ";
			tempStr = tempStr.concat((this.amfm)? "AM":"FM");
			String freq = String.valueOf(frequency[temp]/((this.amfm)? 1:100));
			if (!this.amfm){
				String freqSub1 = freq.substring(0,freq.length()-1);
				String freqSub2 = freq.substring(freq.length()-1);
				freq = freqSub1 + "." + freqSub2;
			}
			for (int i = 0; i < 7-freq.length(); i++) {
				tempStr = tempStr.concat(" ");
			}
			tempStr = tempStr.concat(freq + "  ");
			String stationName = this.stationData.lookupFreq((this.amfm)? FreqBand.AM:FreqBand.FM, frequency[temp]);
			if (stationName == null){
				tempStr = tempStr.concat("****  |");
			} else {
				tempStr = tempStr.concat(stationName + "  |");
			}
			lines.add(tempStr);
			String tempStr2 = "|  Vol:";
			if (this.muted) {
				tempStr2 = tempStr2.concat(" --    ");
			} else {
				if (this.volume > 9) {
					tempStr2 = tempStr2.concat(" " + this.volume + "    ");
				} else {
					tempStr2 = tempStr2.concat("  " + this.volume + "    ");
				}
			}
			if (this.freqSet == 1) {
				tempStr2 = tempStr2.concat("SET1  |");
			} else if (this.freqSet == 2) {
				tempStr2 = tempStr2.concat("SET2  |");
			} else {
				tempStr2 = tempStr2.concat("      |");
			}
			lines.add(tempStr2);
			lines.add("---------------------");
		}
		return lines;
	}
}
