/**
 * Jam.java
 * 
 * File:
 *   $Id: Jam.java,v 1.1.2.2 2013/05/03 00:55:50 p243-07b Exp $
 *   
 * Revisions:
 *   $Log: Jam.java,v $
 *   Revision 1.1.2.2  2013/05/03 00:55:50  p243-07b
 *   finished with the project. comments and all.
 *
 *   Revision 1.1.2.1  2013/05/02 06:00:04  p243-07b
 *   finally done with a boat load of code.  need to comment and polish everything up
 *
 *
 */

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.BevelBorder;

/**
 * view of jam game
 * 
 * @author - Joseph Gambino <jdg7026@rit.edu>
 */

public class Jam extends JFrame implements Observer{
	
	private JamModel model;
	private ArrayList<CarButton> carButtons;
	private ArrayList<Color> colors;
	private JLabel moveCount;
	private JLabel notification;
	private JLabel carSelect;
	private JPanel topBar;
	private JPanel carButtonPanel;
	private JPanel bottomBar;
	private JOptionPane winBox;
	private JOptionPane help;
	private boolean beginning;
	
	/**
	 * constructs a new Jam view based on the given model.
	 * @param model - the model of the jam game
	 */
	public Jam(JamModel model){
		super();
		this.model = model;
		model.addObserver(this);
		this.setLayout(new BorderLayout());
		this.setBackground(Color.BLACK);
		
		//creates the top bar and all of its labels
		this.moveCount = new JLabel("Moves: ");
		this.carSelect = new JLabel("Welcome to Traffic Jam!" +
				" Click help for more information.");
		this.beginning = true;
		this.carSelect.setHorizontalAlignment(JLabel.CENTER);
		this.notification = new JLabel();
		this.topBar = new JPanel();
		this.topBar.setLayout(new BorderLayout());
		this.topBar.add(this.moveCount, BorderLayout.WEST);
		this.topBar.add(this.carSelect, BorderLayout.NORTH);
		this.topBar.add(this.notification, BorderLayout.EAST);
		
		add(topBar, BorderLayout.NORTH);
		
		//creates car button jpanel, and populates it with buttons
		this.carButtons = new ArrayList<CarButton>();
		this.carButtonPanel = new JPanel();
		
		int[] rc = model.getDimensions();
		this.carButtonPanel.setLayout(new GridLayout(rc[0],rc[1]));
		
		ArrayList<Integer> config = this.model.getConfig();
		for( int i = 0; i < config.size(); i++ ){
			CarButton newButton = new CarButton(i);
			newButton.setBorder(
				new BevelBorder(BevelBorder.RAISED, 
								Color.GRAY, 
								Color.LIGHT_GRAY));
			newButton.addActionListener(new CarButtonListener());
			carButtons.add(newButton);
			carButtonPanel.add(newButton);
		}
		
		add(carButtonPanel, BorderLayout.CENTER);
		
		// creates an array list of random colors
		colors = new ArrayList<Color>();
		for( int i = 0; i < model.getNumCars(); i++ )
			colors.add(genColor());
			
		//creates bottom bar and buttons
		this.bottomBar = new JPanel();
		this.bottomBar.setLayout(new FlowLayout(FlowLayout.RIGHT));
		JButton reset = new JButton("Reset");
		reset.addActionListener(new ResetListener());
		JButton undo = new JButton("Unselect Car");
		undo.addActionListener(new UndoListener());
		JButton nextMove = new JButton("Next Move");
		nextMove.addActionListener(new NextMoveListener());
		JButton quit = new JButton("Quit");
		quit.addActionListener(new QuitListener());
		JButton help = new JButton("Help");
		help.addActionListener(new HelpListener());
		this.bottomBar.add(nextMove);
		this.bottomBar.add(undo);
		this.bottomBar.add(reset);
		this.bottomBar.add(help);
		this.bottomBar.add(quit);
		
		add(bottomBar, BorderLayout.SOUTH);
		
		this.help = new JOptionPane();
		this.winBox = new JOptionPane();
		
		// sets some options for the window, like default size and title
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Jam Joseph Gambino <jdg7026>");
		setSize(800,600);
		setLocationRelativeTo(null);
		setVisible(true);
		// updates the view based on the initial model
		update(model, this);
	}
	
	/**
	 * generates a random color that is not close to red
	 * @return - the random color
	 */
	private Color genColor(){
		Random rand = new Random();
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();
		if( r-50 > g || r-50 > b ) return genColor();
		else return new Color(r, g, b);
	}
	
	/**
	 * constructs a jam view, beginning the game.
	 * @param args - the command line arguments. args[0] is the file name
	 */
	public static void main(String[] args){
		if( args.length != 1 ){
			System.out.println("usage: java Jam input-file");
		} else{
			new Jam(new JamModel(args[0]));
		}
	}

	/**
	 * updates the view based on the current state of the model
	 * @param arg0 - the model, not used
	 * @param arg1 - the view, not used
	 */
	public void update(Observable arg0, Object arg1) {
		int selectedCar = model.getSelectedCar();
		// checks all of the cars
		ArrayList<Integer> board = model.getConfig();
		for( int i = 0; i < board.size(); i++ ){
			int curr = board.get(i);
			CarButton carButton = carButtons.get(i);
			if( curr >= 0 ){
				if( curr == selectedCar )carButton.setBackground(Color.RED);
				else carButton.setBackground(colors.get(curr));
				carButton.setBorderPainted(false);
				carButton.occupy(curr);
				if( curr == colors.size()-1 )  
					carButton.setText("Escape");
				else carButton.setText(""+curr);
			} else {
				carButton.setBorderPainted(true);
				carButton.setBackground(Color.WHITE);
				carButton.occupy(-1);
				carButton.setText("");
			}
		}
		
		// checks the text labels
		if( !model.isGoal( model.getConfig() ) ){	
			moveCount.setText("Moves: " + model.getNumMoves());
			String move = (selectedCar < 0)? 
				"Select a Car to move.":
				"Choose" + " a space to move to.";
			notification.setText(move);
			if( selectedCar != -1 ){
				this.carSelect.setText("Car " + selectedCar + " selected. ");
			} else {
				if ( !beginning ) this.carSelect.setText(" ");
			}
		} else {
			int response = 
				this.winBox.showConfirmDialog(this, 
					  "Would you like to play again?",
					  "Congratulations! You've won!", 
					  JOptionPane.YES_NO_OPTION);
			if( response == JOptionPane.YES_OPTION ){
				model.reset();
			} else {
				this.dispose();
			}
		}
	}
	
	/**
	 * listener for the car buttons
	 */
	public class CarButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if( beginning ) beginning = !beginning;
			CarButton carButton = (CarButton)e.getSource();
			int selectedCar = model.getSelectedCar();
			int occupied = carButton.occupied();
			
			// car has already been selected, now a move should be made
			if( occupied < 0 && selectedCar >= 0 ){
				Car selected = model.getCars().get(selectedCar);
				int forward = 0;
				ArrayList<Integer> cars = model.getConfig();
				for( int i = 0; i < cars.size(); i++ ){
					if( cars.get(i) == selected.getNum() ){
						int buttonPos = carButton.getPos();
						if( selected.isVertical() ){
							if( buttonPos + model.getDimensions()[1] == i){
								forward = -1;
							} else if( buttonPos -
									model.getDimensions()[1] == i ){
								forward = 1;
							}
						} else {
							if( buttonPos + 1 == i){
								forward = -1;
							} else if( buttonPos - 1 == i ){
								forward = 1;
							}
						}
					}
				}
				if( forward != 0 ){
					boolean move;
					if( forward == 1 ){
						move = true;
					} else {
						move = false;
					}
					model.moveCar(selectedCar, move);
				} else {
					carSelect.setText("Invalid Location.");
				}
			// car has not been selected, now a car should be selected
			} else if( occupied >= 0 && selectedCar < 0 ){
				model.selectCar(occupied);
			} else { 
				carSelect.setText("Invalid Move.");
			}
		}
	}
	
	/**
	 * inner class for a reset button
	 */
	public class ResetListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			//resets the game in the model
			model.reset();
		}
	}
	
	/**
	 * inner class for an undo button
	 */
	public class UndoListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			// undo the last move made
			model.selectCar(-1);
		}
	}
	
	/**
	 * inner class for next best move
	 */
	public class NextMoveListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			// make the next best move
			model.nextMove();
		}
	}
	
	/**
	 * inner class to quit
	 */
	public class QuitListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			// exit game
			dispose();
		}
	}
	
	/**
	 * inner class for a help button
	 */
	public class HelpListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String helpMsg =
				"Welcome to Traffic Jam!\n\n" +
				"The goal of this game is to move" +
				" the car marked \'Escape\' to the" +
				" far right column of the grid.\n\n" +
				"Click on a car to select it, " +
				"then click on a destination. " +
				"Destinations can only be one\n" +
				"square at a time to the north " +
				"or south of vertical cars " +
				"and east or west of horizontal cars.\n\n" +
				"Click Unselect Car to deselect " +
				"the currently selected car.\n\n" +
				"Click Next Move to have the " +
				"next best move automatically " +
				"made for you.\n\n" +
				"Click Help to show this dialogue.\n\n" +
				"Click Reset to reset the game " +
				"to the original configuration.\n\n" +
				"Click Quit to quit the game.\n\n";
			
			// produce dialogue for help
			help.showMessageDialog(null, helpMsg);
		}
	}
}