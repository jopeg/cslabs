/**
 * Clock.java
 *
 * File:
 * 	$Id: Clock.java,v 1.1.2.1 2013/05/03 00:55:49 p243-07b Exp $
 *
 * Revisions:
 *  $Log: Clock.java,v $
 *  Revision 1.1.2.1  2013/05/03 00:55:49  p243-07b
 *  finished with the project. comments and all.
 *
 *  Revision 1.1  2013/04/22 22:06:27  p243-07b
 *  rewrote most of the code, solver finally works.  clock and water also work.  ready for part 3.
 *
 *  Revision 1.2  2013/04/04 19:50:57  jdg7026
 *  added checking for start/goal times being out of range, wordy, but works well.
 *
 *  Revision 1.1  2013/04/04 19:33:35  jdg7026
 *  done with the whole thing
 *
 *
 *  @author Joseph Gambino <jdg7026@rit.edu>
 */

import java.util.*;

/**
 * the Clock puzzle
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public class Clock implements Puzzle{
	private int hours;
	private ArrayList<Integer> start;
	private int goal;
	
	/**
	 * constructs a Clock object
	 *
	 * @param String[] args = the command line arguments [hours, start, goal]
	 */
	private Clock(String[] args){
		ArrayList<Integer> new_start = new ArrayList<Integer>(1);
		new_start.add( Integer.valueOf( args[1] ) );

		this.hours = Integer.valueOf(args[0]); 
		this.start = new_start;
		this.goal = Integer.valueOf(args[2]); 
	}
	/**
	 * the main method, creates the clock and solves it, then prints the solution.
	 *
	 * @param String[] args = the command line arguments [hours, start, goal]
	 */
	public static void main(String[] args){
		if (args.length != 3){
			System.out.println("Usage: java Clock hours start goal");
		} else {
			Clock clock = new Clock(args);
			if (clock.start.get(0) < clock.hours || clock.goal < clock.hours || clock.start.get(0) > 1 || clock.goal > 1){
				ArrayList<Collection<Integer>> steps = (ArrayList<Collection<Integer>>) Solver.solve(clock);
				for (int i = 0; i < steps.size(); i++){
					System.out.println("Step " + i + ": " + ((ArrayList<Integer>) steps.get(i)).get(0));
				}
			} else {
				System.out.println("start and/or goal time out of range");
			}
		}
	}
	/**
	 * @return int representing the hour time
	 */

    public boolean isGoal(Collection<Integer> config){
        for( Integer i : config ){
            if( i == this.goal ){ return true; }
        }
        return false;
    }

	public int getGoal(){
		return this.goal;
	}
	/**
	 * generates an ArrayList<Integer> of size 2 with the neighbors of a given hour
	 *
	 * @param int config.get(0) = hour to get neighbors of
	 *
	 * @return ArrayList<Integer> containing the neighbors of the supplied hour
	 */
	public Collection<Collection<Integer>> getNeighbors(Collection<Integer> inconfig) {
		ArrayList<Integer> config = (ArrayList<Integer>) inconfig;
		Collection<Collection<Integer>> neighbors = new ArrayList<Collection<Integer>>(2);

		Collection<Integer> one = new ArrayList<Integer>(1);
		Collection<Integer> two = new ArrayList<Integer>(1);

		if (config.get(0) == this.hours){
			one.add(1);
			two.add(config.get(0)-1);
		} else if (config.get(0) == 1){
			one.add(this.hours);
			two.add(config.get(0)+1);
		} else {
			one.add(config.get(0)-1);
			two.add(config.get(0)+1);
		}
		
		neighbors.add( one );
		neighbors.add( two );

		return neighbors;
	}
	/**
	 * @return int representing the start hour
	 */
	public Collection<Integer> getStart(){
		return this.start;
	}
}