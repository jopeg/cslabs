/**
 * Water.java
 * 
 * File:
 *   $Id: Water.java,v 1.1.2.2 2013/05/03 00:55:48 p243-07b Exp $
 *   
 * Revisions:
 *   $Log: Water.java,v $
 *   Revision 1.1.2.2  2013/05/03 00:55:48  p243-07b
 *   finished with the project. comments and all.
 *
 *   Revision 1.1.2.1  2013/05/02 06:00:00  p243-07b
 *   finally done with a boat load of code.  need to comment and polish everything up
 *
 *
 */

import java.util.*;

/**
 * the Water puzzle
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public class Water implements Puzzle{
    /**
     * constructs the puzzle from the input then calls the solver
     * and prints the solution
     * 
     * @param args
     */
    public static void main(String args[]){
        if (args.length < 2){
            System.out.println("Usage: java Water amount jug1 jug2 ...");
            return;
        }

        ArrayList<Integer> jugs = new ArrayList<Integer>();

        for( int i = 1; i < args.length; i++ )
            jugs.add(Integer.parseInt(args[i]));

        Water w = new Water(Integer.parseInt(args[0]), jugs);
        Collection<Collection<Integer>> insolution = Solver.solve(w);

		if ( insolution != null ){
            ArrayList<Collection<Integer>> solution = 
                            new ArrayList<Collection<Integer>>(insolution);
			for ( int j = 0; j < solution.size(); j++){
				ArrayList<Integer> step = (ArrayList<Integer>)solution.get(j);
				System.out.print("Step " + j + ": ");
				for( int k : step ){
					System.out.print(k + " ");
				}
				System.out.print("\n");
			}
		} else {
			System.out.println("No solution.");
		}


    }

    private int amount;
    private ArrayList<Integer> jug_capacity;
    private ArrayList<Integer> jug_amount;

    /**
     * constructs a water puzzle with the given amount goal and
     * jug capacities
     * @param amount - amount of water desired in one jug at the goal
     * @param jugs - the list of jug capacities
     */
    public Water(int amount, ArrayList<Integer> jugs) {
        this.amount = amount;
        this.jug_capacity = jugs;
        this.jug_amount = (ArrayList<Integer>)getStart();
    }

    /**
     * @param config - config to check for goal
     * @return - true if the given config 
     * 			collection contains the desired amount
     */
    public boolean isGoal(Collection<Integer> config){
        for( Integer i : config ){
            if( i == amount ){ return true; }
        }
        return false;
    }

    /**
     * @return the goal amount of water
     */
    public int getGoal() {
        return amount;
    }

    /**
     * @return the starting config of the puzzle
     */
    public Collection<Integer> getStart() {
        ArrayList<Integer> start = new ArrayList<Integer>();

        for( int i = 0; i < jug_capacity.size(); i++ )
            start.add( 0 );

        return start;
    }

    /**
     * generates the neighbors of the given config
     * @param inconfig - the config to get the neighbors of
     * @return - the collection of valid neighbor configs
     */
    public Collection<Collection<Integer>> 
    		getNeighbors(Collection<Integer> inconfig) {
    	ArrayList<Integer> config = new ArrayList<Integer>(inconfig);
    	ArrayList<Collection<Integer>> neighbors = 
    			new ArrayList<Collection<Integer>>();
    	
    	for( int curr = 0; curr < config.size(); curr++ ){
    		int currJug = config.get(curr); 
    		ArrayList<Integer> newConfig = new ArrayList<Integer>(config);
    		if( currJug != jug_capacity.get(curr) ){
    			newConfig.set(curr, this.jug_capacity.get(curr));
    			neighbors.add(new ArrayList<Integer>(newConfig));
    			newConfig = new ArrayList<Integer>(config);
    		}
    		
    		if( currJug != 0 ){
    			newConfig.set(curr, 0);
    			neighbors.add(new ArrayList<Integer>(newConfig));
    			newConfig = new ArrayList<Integer>(config);    		
    			for( int next = 0; next < config.size(); next++ ){
    				if( curr == next ) continue;
    				int freeSpace = 
    						this.jug_capacity.get(next) - config.get(next);
    				if( freeSpace > 0 ){
    					int amountRemaining = config.get(curr) - freeSpace;
    					if( amountRemaining >= 0 ){
    						newConfig.set(curr, amountRemaining);
    						newConfig.set(next, this.jug_capacity.get(next));
    					} else {
    						newConfig.set(curr, 0);
    						newConfig.set(next, config.get(next)+config.get(curr));
    					}
    					neighbors.add(new ArrayList<Integer>(newConfig));
    					newConfig = new ArrayList<Integer>(config);
    				}
    			}
    		}
    	}
    	return neighbors;
    }
}