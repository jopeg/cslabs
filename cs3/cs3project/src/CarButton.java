/**
 * CarButton.java
 * 
 * File:
 *   $Id: CarButton.java,v 1.1.2.2 2013/05/03 00:55:49 p243-07b Exp $
 *   
 * Revisions:
 *   $Log: CarButton.java,v $
 *   Revision 1.1.2.2  2013/05/03 00:55:49  p243-07b
 *   finished with the project. comments and all.
 *
 *   Revision 1.1.2.1  2013/05/02 06:00:03  p243-07b
 *   finally done with a boat load of code.  need to comment and polish everything up
 *
 *
 */
import java.awt.Color;

import javax.swing.JButton;

/**
 * Button for a car in the jam view
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public class CarButton extends JButton{
	private int pos;
	private int occupied;
	
	/**
	 * constructs a car button at a given position
	 * @param pos - position of the button on the grid
	 */
	public CarButton(int pos){
		this.pos = pos;
		this.occupied = -1;
		//sets some default options
		this.setBorderPainted(false);
		this.setContentAreaFilled(false);
		this.setOpaque(true);
		this.setForeground(Color.BLACK);
		this.setBackground(Color.WHITE);
	}
	
	/**
	 * @return - the position of this button
	 */
	public int getPos(){
		return this.pos;
	}
	
	/**
	 * @return - the number of the car that is on this button. -1 if none. 
	 */
	public int occupied(){
		return this.occupied;
	}
	
	/**
	 * sets the occupant of this button
	 * @param - the number of the car that now occupys this button
	 */
	public void occupy(int carNum){
		this.occupied = carNum;
	}
}