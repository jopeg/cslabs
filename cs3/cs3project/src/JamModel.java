/**
 * JamModel.java
 * 
 * File:
 *   $Id: JamModel.java,v 1.1.2.3 2013/05/03 00:55:49 p243-07b Exp $
 *   
 * Revisions:
 *   $Log: JamModel.java,v $
 *   Revision 1.1.2.3  2013/05/03 00:55:49  p243-07b
 *   finished with the project. comments and all.
 *
 *   Revision 1.1.2.2  2013/05/02 06:00:01  p243-07b
 *   finally done with a boat load of code.  need to comment and polish everything up
 *
 *
 */
import java.io.*;
import java.util.*;
/**
 * model for the traffic jam game
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public class JamModel extends Observable implements Puzzle{
	private int rows;
	private int cols;
	private int numCars;
	private ArrayList<Car> cars;
	private ArrayList<Car> tempCars;
	private ArrayList<Car> startCars;
	private Car[][] grid;
	private int numMoves;
	private int carSelected;
	
	/**
	 * constructs a jam model based on the given input file
	 * @param fileName - name of text file representing initial configuration
	 */
	public JamModel(String fileName){
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileName));
			
			//Read line 1, (Rows, Columns)
			String next_line = in.readLine();
			String[] rowcol = next_line.split(" ");
			this.rows = Integer.valueOf( rowcol[0] );
			this.cols = Integer.valueOf( rowcol[1] );
			
			//Read line 2, (Number of Cars)
			next_line = in.readLine();
			this.numCars = Integer.valueOf(next_line.split(" ")[0]);
			
			//Read in all cars
			this.cars = new ArrayList<Car>();
			for( int i = 0; i < this.numCars; i++ ){
				next_line = in.readLine();
				ArrayList<Integer> pos = new ArrayList<Integer>(4);
				for( String n : next_line.split(" ") )
					pos.add(Integer.valueOf(n));
				this.cars.add(new Car(pos, i));
			}
			in.close();
			
			numMoves = 0;
			carSelected = -1;
			
			startCars = deepCopyCars(cars);
	
			grid = new Car[rows][cols];
			grid = updateGrid(grid, cars);
			
		} catch(FileNotFoundException e) {
			System.err.println(fileName + " not found.");
			e.printStackTrace();
		} catch(IOException e){
			e.printStackTrace();
		}
	}
	

	/**
	 * updates a given 2d Car array with the cars in the given cars list
	 * @param carsArray - 2d array of cars representing the board to be updated
	 * @param carsList - list of cars to update the board to
	 * @return - the updated Car array
	 */
	private Car[][] updateGrid(Car[][] carsArray, ArrayList<Car> carsList){
			Car[][] tmpArray = new Car[rows][cols];
			for( Car c : carsList ){
				for( int i = 0; i < c.getLen(); i++ ){
					ArrayList<Integer> pos = c.getPos();
					if( c.isVertical() ){
						int col = pos.get(1);
						tmpArray[pos.get(0)+i][col] = c;
					} else {
						int row = pos.get(0);
						tmpArray[row][pos.get(1)+i] = c;
					}
				}
			}
			carsArray = new Car[rows][cols];
			for( int i = 0; i < tmpArray.length; i++ ){
				carsArray[i] = tmpArray[i].clone();
			}
			return tmpArray;
		}

	
	/**
	 * converts an array list of integers to a 2d array of cars. used to
	 * convert configs given from the solver
	 * @param carsAL - array list of integers representing the current board
	 * @return - a 2d array of Car objects representing the board
	 */
	public Car[][] arrayListToGrid(ArrayList<Integer> carsAL){
		Car[][] carsArray = new Car[rows][cols];
		
		HashMap<Integer, ArrayList<int[]>> newCars = 
				new HashMap<Integer, ArrayList<int[]>>();
		
		for(int k = 0; k < numCars; k++)
			newCars.put(k, new ArrayList<int[]>());
		
		int index = 0;
		for( int i = 0; i < rows; i++ ){
			for( int j = 0; j < cols; j++ ){
				int num = carsAL.get(index);
				if( num >= 0 ){
					int[] coord = {i,j};
					newCars.get(num).add(coord);
				}
				index++;
			}
		}
		
		tempCars = new ArrayList<Car>();
		
		for( int l = 0; l < numCars; l++ ){
			ArrayList<Integer> pos = new ArrayList<Integer>();
			ArrayList<int[]> coords = newCars.get(l);
			pos.add(coords.get(0)[0]);
			pos.add(coords.get(0)[1]);
			pos.add(coords.get(coords.size()-1)[0]);
			pos.add(coords.get(coords.size()-1)[1]);
			tempCars.add(new Car(pos, l));
		}
		
		return updateGrid(carsArray, tempCars);
	}


	/**
	 * converts a car 2d array to an array list of integers for the solver
	 * @param carsArray - a 2d array of Car objects representing a board config
	 * @return - array list of integers representing the board config
	 */
	private ArrayList<Integer> gridToArrayList(Car[][] carsArray){
		ArrayList<Integer> aL = new ArrayList<Integer>();
		for( Car[] row : carsArray ){
			for( Car c : row ){
				if( c == null ){
					aL.add(-1);
				} else {
					aL.add(c.getNum());
				}
			}
		}
		return aL;
	}
	
	
	/**
	 * creates a deep copied array list containing all new car objects
	 * @param cars - array list of cars to be copied
	 * @return - the copied cars array list
	 */
	private ArrayList<Car> deepCopyCars(ArrayList<Car> cars){
		ArrayList<Car> newArray = new ArrayList<Car>();
		for( int i = 0; i < cars.size(); i++ )
			newArray.add(new Car(cars.get(i)));
		
		return newArray;
	}
	
	
	/**
	 * moves the given car number forwards/backwards and updates the grid
	 * @param num - number of the car to move
	 * @param forward - true if moving forward, false if backward
	 * @return - true if move was valid and occurred, false otherwise
	 */
	public boolean moveCar(int num, boolean forward){
		ArrayList<Collection<Integer>> possibleMoves = 
				(ArrayList<Collection<Integer>>)getNeighbors(
						gridToArrayList(this.grid));
		ArrayList<Car> copyCars = deepCopyCars(cars);
		Car carTest = copyCars.get(num);
		carTest.move(forward);
		Car[][] copyGrid = new Car[rows][cols];
		copyGrid = updateGrid(copyGrid, copyCars);
		if( possibleMoves.contains(gridToArrayList(copyGrid)) ){
			Car car = cars.get(num);
			car.move(forward);
			grid = updateGrid(grid, cars);
			numMoves++;
            this.carSelected = -1;
			setChanged();
			notifyObservers();
			return true;
		} 
		return false;
	}
	
	/**
	 * uses the solver to solve the puzzle and automatically updates the
	 * game to include the best move possible
	 * @return - true if move was found and used, false otherwise
	 */
	public boolean nextMove(){
		ArrayList<Collection<Integer>> solution = 
				(ArrayList<Collection<Integer>>)Solver.solve(this);
		if ( solution != null ){
            grid = arrayListToGrid((ArrayList<Integer>)solution.get(1));
            cars = deepCopyCars(tempCars);
            numMoves++;
            this.carSelected = -1;
            setChanged();
			notifyObservers();
			return true;
		}
		return false;
	}
	
	/**
	 * selects a car
	 * @param num - car to be selected
	 */
	public void selectCar(int num){
		this.carSelected = num;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * resets the game to the starting config
	 */
	public void reset(){
		grid = updateGrid(grid, startCars);
		cars = deepCopyCars(startCars);
		numMoves = 0;
		carSelected = -1;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * @return - is a car selected?
	 */
	public int getSelectedCar(){
		return this.carSelected;
	}
	
	/** 
	 * @return - the number of moves that have been performed
	 */
	public int getNumMoves(){
		return this.numMoves;
	}

	/**
	 * @return - the number of cars in the game
	 */
	public int getNumCars(){
		return this.numCars;
	}
	
	/**
	 * @return - the current board config as an array list of integers
	 */
	public ArrayList<Integer> getConfig(){
		return gridToArrayList(this.grid);
	}
	
	/**
	 * @return - the current list of cars
	 */
	public ArrayList<Car> getCars(){
		return this.cars;
	}
	
	/**
	 * @return the goal column
	 */
	public int getGoal() {
		return this.cols;
	}

	/**
	 * @return the rows, cols of the board
	 */
	public int[] getDimensions(){
		int[] rc = {rows, cols};
		return rc;
	}
	
	/**
	 * @return - the current board state, used to get next best move
	 */
	public Collection<Integer> getStart() {
		return gridToArrayList(this.grid);
	}

	
	/**
	 * calculates all valid neighbors of a given config
	 * @param config - collection of integers representing a game board
	 * @returns - collection of board configs as collection of integers
	 */
	public Collection<Collection<Integer>> getNeighbors(
			Collection<Integer> config) {
		ArrayList<Collection<Integer>> neighbors = 
				new ArrayList<Collection<Integer>>(); 
		Car[][] currConfig = arrayListToGrid((ArrayList<Integer>)config);
		Car[][] nextConfig = new Car[rows][cols];
		
		ArrayList<Car> newCars = deepCopyCars( this.tempCars );
		
		for( int c = 0; c < newCars.size(); c++ ){
			Car car = newCars.get(c);
			ArrayList<Integer> pos = car.getPos();
			if( car.isVertical() ){
				if( pos.get(0) != 0 && 
						currConfig[pos.get(0)-1][pos.get(1)] == null ){
					car.move(false);
					neighbors.add(gridToArrayList(
							nextConfig = updateGrid(nextConfig, newCars)));
					car.move(true);
				}
				
				if( pos.get(2) != rows-1 &&
						currConfig[pos.get(2)+1][pos.get(1)] == null){
					car.move(true);
					neighbors.add(gridToArrayList(
							nextConfig = updateGrid(nextConfig, newCars)));
					car.move(false);
				}
			} else {
				if( pos.get(1) != 0 && 
						currConfig[pos.get(0)][pos.get(1)-1] == null ){
					car.move(false);
					neighbors.add(gridToArrayList(
							nextConfig = updateGrid(nextConfig, newCars)));
					car.move(true);
				}
				
				if( pos.get(3) != cols-1 && 
						currConfig[pos.get(0)][pos.get(3)+1] == null){
					car.move(true);
					neighbors.add(gridToArrayList(
							nextConfig = updateGrid(nextConfig, newCars)));
					car.move(false);
				}
			}
		}
		
		return neighbors;
	}

	
	/**
	 * checks if the given configuration is the goal
	 * 
	 * @param config - a collection of integers representing the config to be
	 * 					checked
	 * @return - true if config is goal, false otherwise
	 */
	public boolean isGoal(Collection<Integer> config) {
		Car[][] newCars  = arrayListToGrid((ArrayList<Integer>)config);
		for( int i = 0; i < rows; i++ ){
			for( int j = 0; j < cols; j++ ){
				Car c = newCars[i][j];
				if( c != null ){
					if( c.getNum() == numCars-1 ){
						ArrayList<Integer> pos = c.getPos();
						if( pos.get(3) == this.cols-1 ) return true;
						else return false;
					}
				}
			}
		}
		return false;
	}
	
	
	/**
	 * prints a text based representation of a given grid
	 * @param grid - 2d Car array representing current board
	 */
	public void printTextGrid(Car[][] grid){
		for(Car[] r : grid){
			for(Car c : r){
				if(c != null){
					System.out.print(c.getNum() + " ");
				} else {
					System.out.print(". ");
				}
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}
}