/**
 * Solver.java
 * 
 * File:
 *   $Id: Solver.java,v 1.1.2.2 2013/05/03 00:55:49 p243-07b Exp $
 *   
 * Revisions:
 *   $Log: Solver.java,v $
 *   Revision 1.1.2.2  2013/05/03 00:55:49  p243-07b
 *   finished with the project. comments and all.
 *
 *   Revision 1.1.2.1  2013/05/02 06:00:01  p243-07b
 *   finally done with a boat load of code.  need to comment and polish everything up
 *
 *
 */
import java.util.*;

/**
 * generic puzzle solver
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public class Solver {
	
	/**
	 * solves the given puzzle using a BFS
	 * @param puzzle - a puzzle object
	 * @return - the solution steps in order
	 */
	public static Collection<Collection<Integer>> solve(Puzzle puzzle){
		ArrayList<ArrayList<Integer>> queue = 
				new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> start = 
				new ArrayList<Integer>(puzzle.getStart());
		ArrayList<ArrayList<Integer>> visited = 
				new ArrayList<ArrayList<Integer>>();
		HashMap<ArrayList<Integer>, ArrayList<Integer>> preds =
				new HashMap<ArrayList<Integer>, ArrayList<Integer>>();
		
		queue.add(start);
		visited.add(start);
		preds.put(start, start);
		
		boolean done = false;
		
		while( !queue.isEmpty() && !done ){
			ArrayList<Integer> curr = queue.remove(0);
			ArrayList<Collection<Integer>> neighbors = 
				new ArrayList<Collection<Integer>>(puzzle.getNeighbors(curr));
			for( int i = 0; i < neighbors.size(); i++ ){
				ArrayList<Integer> neighbor = 
						new ArrayList<Integer>(neighbors.get(i));
				if( puzzle.isGoal(neighbor) ){
					done = true;
					visited.add(neighbor);
					preds.put(neighbor, curr);
					break;
				} else {
					if ( !visited.contains(neighbor) ){
						queue.add(neighbor);
						visited.add(neighbor);
						preds.put(neighbor, curr);
					}
				}
			}
		}
		// no solution
		if ( done == false ){ return null; }
		
		//the next block of code constructs the solution from the hashmap
		ArrayList<Collection<Integer>> solution = 
				new ArrayList<Collection<Integer>>();
		ArrayList<Integer> current = 
				new ArrayList<Integer>(visited.get(visited.size()-1));
		
		while( current != start ){
			solution.add(current);
			current = preds.get(current);
		}
		solution.add(puzzle.getStart());
		Collections.reverse(solution);
		return solution;
		
	}
}