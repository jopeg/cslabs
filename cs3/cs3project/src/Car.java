/**
 * Car.java
 * 
 * File:
 *   $Id: Car.java,v 1.1.2.3 2013/05/03 00:55:49 p243-07b Exp $
 *   
 * Revisions:
 *   $Log: Car.java,v $
 *   Revision 1.1.2.3  2013/05/03 00:55:49  p243-07b
 *   finished with the project. comments and all.
 *
 *   Revision 1.1.2.2  2013/05/02 06:00:00  p243-07b
 *   finally done with a boat load of code.  need to comment and polish everything up
 *
 *
 */

import java.util.*;

/**
 * Car object for the Jam game
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public class Car{
	private ArrayList<Integer> pos;
	//True for vertical, false for horizontal
	private boolean vertical;
	private int num;
	private int len;
	
	
	/**
	 * constructs a car at the given position with the given number
	 * @param pos - (r1, c1, r2, c2) the start and end coordinates 
	 * 				the car
	 * @param num - the number representation of the car
	 */
	public Car(ArrayList<Integer> pos, int num){
		this.pos = pos;
		if ( pos.get(0) == pos.get(2) ){
			vertical = false;
			this.len = Math.abs( pos.get( 3 ) - pos.get( 1 ) ) + 1;
		} else {
			vertical = true;
			this.len = Math.abs( pos.get( 2 ) - pos.get( 0 ) ) + 1;	
		}
		this.num = num;
	}
	
	/**
	 * constructs a completely new copy of the given car object
	 * @param c - the car object to be copied
	 */
	public Car(Car c){
		this.pos = new ArrayList<Integer>(c.getPos());
		this.vertical = (boolean)(new Boolean(c.isVertical()));
		this.num = (int)(new Integer(c.getNum()));
		this.len = (int)(new Integer(c.getLen()));
	}
	
	/**
	 * @return - the number of the car
	 */
	public int getNum(){
		return this.num;
	}
	
	/**
	 * @return - the length of the car
	 */
	public int getLen(){
		return this.len;
	}
	
	/**
	 * @return - (r1, c1, r2, c2) the position of the car
	 */
	public ArrayList<Integer> getPos(){
		return this.pos;
	}
	
	/**
	 * @return - true or false if the car is vertical
	 */
	public boolean isVertical(){
		return this.vertical;
	}
	
	/**
	 * moves the car forward or backward. assumes move is valid
	 * 
	 * @param forward - true if forward, false if backward
	 * forward is down and right, backward is up and left
	 */
	public void move(boolean forward){
		if( forward ){
			if( !this.vertical ){
				this.pos.set(1, this.pos.get(1)+1);
				this.pos.set(3, this.pos.get(3)+1);
			} else {
				this.pos.set(0, this.pos.get(0)+1);
				this.pos.set(2, this.pos.get(2)+1);
			}
		} else {
			if( !this.vertical ){
				this.pos.set(1, this.pos.get(1)-1);
				this.pos.set(3, this.pos.get(3)-1);
			} else {
				this.pos.set(0, this.pos.get(0)-1);
				this.pos.set(2, this.pos.get(2)-1);
			}
		}
	}
}