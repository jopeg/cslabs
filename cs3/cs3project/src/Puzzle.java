/**
 * Puzzle.java
 * 
 * File:
 * 	$Id: Puzzle.java,v 1.1.2.1 2013/05/03 00:55:50 p243-07b Exp $
 * 
 * Revisions:
 * 	$Log: Puzzle.java,v $
 * 	Revision 1.1.2.1  2013/05/03 00:55:50  p243-07b
 * 	finished with the project. comments and all.
 *
 * 	Revision 1.1  2013/04/22 22:06:27  p243-07b
 * 	rewrote most of the code, solver finally works.  clock and water also work.  ready for part 3.
 *
 * 
 */

import java.util.*;

/**
 * puzzle interface for the solver
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public interface Puzzle{
	//return the goal of the puzzle
	public int getGoal();
	//return the initial config of the puzzle
	public Collection<Integer> getStart();
	//return the neighbors of a given config of the puzzle
	public Collection<Collection<Integer>> 
			getNeighbors(Collection<Integer> config);
	//return true or false if the given config is the goal
	public boolean isGoal(Collection<Integer> config);
}