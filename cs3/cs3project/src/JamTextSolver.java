/**
 * JamTextSolver.java
 * 
 * File:
 *   $Id: JamTextSolver.java,v 1.1.2.2 2013/05/03 00:55:50 p243-07b Exp $
 *   
 * Revisions:
 *   $Log: JamTextSolver.java,v $
 *   Revision 1.1.2.2  2013/05/03 00:55:50  p243-07b
 *   finished with the project. comments and all.
 *
 *   Revision 1.1.2.1  2013/05/02 06:00:04  p243-07b
 *   finally done with a boat load of code.  need to comment and polish everything up
 *
 *
 */

import java.util.ArrayList;
import java.util.Collection;

/**
 * text based UI for solving a given jam puzzle
 * 
 * @author Joseph Gambino <jdg7026@rit.edu>
 *
 */

public class JamTextSolver{
	/**
	 * solves the puzzle
	 * @param args - command line arguments. args[0] is the file name
	 */
	public static void main(String[] args){
		if( args.length != 1 ){
			System.out.println("usage: java JamTextSolver input-file");
		} else {
			try{
				JamModel model = new JamModel(args[0]);
				Collection<Collection<Integer>> insolution = Solver.solve(model);
				if ( insolution != null ){
		            ArrayList<Collection<Integer>> solution = 
		                            new ArrayList<Collection<Integer>>(insolution);
					for ( int j = 0; j < solution.size(); j++){
						ArrayList<Integer> step = 
								(ArrayList<Integer>)solution.get(j);
						System.out.println("Step " + j + ": ");
						model.printTextGrid(model.arrayListToGrid(step));
						System.out.print("\n");
					}
				} else {
					System.out.println("No solution.");
				}
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}
}